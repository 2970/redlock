# redlock

一款基于redis的分布式锁，可以用于解决高并发情况下重复写的情况，比如网络卡的情况下，
用户发表动态，疯狂点击发表按钮，导致请求几乎同时到达服务端，最终导致同样的动态重复发表多次。


# 使用方法

* 安装

```shell
composer require usual2970/redlock
```

* 使用

```php
use usual2970/redlock;

$lock = new redlock\Lock('tcp://redis-address');

$key = get_your_key();
$lock->lock($key);

//你的业务代码

$lock->unLock($key);
```