<?php

namespace usual2970\redlock;

use Predis;

class Lock
{
    public $timeout = 200;
    private $client;
    private $randomStr;

    const LOCK_PREFIX = "LOCK_";

    public function __construct($addr)
    {
        $this->client = new Predis\Client($addr);
        $this->randomStr = $this->randomStr();
    }
    /**
     * 加锁
     *
     * @param [type] $key
     * @return boolean
     */
    public function lock($key)
    {
        $rs = $this->client->set(self::LOCK_PREFIX . $key, $this->randomStr, 'NX', 'PX', $this->timeout);

        return $rs ? true : false;

    }

    /**
     * 解锁
     *
     * @param [type] $key
     * @return void
     */
    public function unLock($key)
    {
        $val = $this->client->get(self::LOCK_PREFIX . $key);
        if ($this->randomStr === $val) {
            $this->client->del(self::LOCK_PREFIX . $key);
        }
    }

    private function randomStr()
    {
        return md5(microtime() . rand(10000000, 99999999));
    }
}
