<?php
require dirname(__FILE__) . '/vendor/autoload.php';
use usual2970\redlock\Lock;
$lock = new Lock("tcp://127.0.0.1:16379");

$rs = $lock->lock('test');
if ($rs) {
    echo 'get lock successed\n';
} else {
    echo 'get lock failed\n';
}

$rs = $lock->unLock('test');
